package gui;



import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MailInterface extends JFrame {

    private JFrame mainFrame = new JFrame("Send mail");
    private static final int FRAME_WIDTH = 800;
    private static final int FRAME_HEIGHT = 600;
    private static final int FRAME_MIN_WIDTH = 640;
    private static final int FRAME_MIN_HEIGHT = 480;

    public MailInterface() {

        final GraphicsDevice gD = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        final int screenCenterX = gD.getDisplayMode().getWidth()/2;
        final int screenCenterY = gD.getDisplayMode().getHeight()/2;

        final int frameWidth  = Math.max(FRAME_MIN_WIDTH, screenCenterX);
        final int frameHeight = Math.max(FRAME_MIN_HEIGHT, screenCenterY);

        final int frameOffsetLeft = screenCenterX - frameWidth/2;
        final int frameOffsetTop = screenCenterY - frameHeight/2;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        mainFrame.setSize(new Dimension(frameWidth, frameHeight));
        mainFrame.setLocation(frameOffsetLeft, frameOffsetTop);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        mainFrame.setPreferredSize(new Dimension(FRAME_WIDTH * 2, FRAME_HEIGHT * 2));
        mainFrame.setResizable(true);
        JPanel bgPanel = new JPanel(new GridBagLayout());
        GridBagConstraints constraints= new GridBagConstraints();
        mainFrame.add(bgPanel);

        JButton sendButton = new JButton("Send button");
        constraints.insets = new Insets(5, 5, 5, 5);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.gridheight = 1;
        constraints.weightx = 1.0;
        constraints.weighty = 0.001;
        constraints.anchor = GridBagConstraints.LINE_START;
        constraints.fill = GridBagConstraints.NONE;
        bgPanel.add(sendButton, constraints);


        JLabel fromLabel = new JLabel("From: ");
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        constraints.weightx = 0.01;
        constraints.weighty = 0.02;
        constraints.anchor = GridBagConstraints.NORTHWEST;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        bgPanel.add(fromLabel, constraints);

        JTextArea fromLine = new JTextArea();
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridheight = 1;
        constraints.weightx = 0.99;
        constraints.weighty = 0.02;
        constraints.fill = GridBagConstraints.BOTH;
        fromLine.setRows(1);
        bgPanel.add(fromLine, constraints);

        JLabel toLabel = new JLabel("To: ");
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridheight = 1;
        constraints.weightx = 0.01;
        constraints.weighty = 0.02;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        bgPanel.add(toLabel, constraints);

        JTextArea toLine = new JTextArea();
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridheight = 1;
        constraints.weightx = 0.99;
        constraints.weighty = 0.02;
        constraints.fill = GridBagConstraints.BOTH;
        toLine.setRows(1);
        bgPanel.add(toLine, constraints);

        JLabel subjectLabel = new JLabel("Subject: ");
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridheight = 1;
        constraints.weightx = 0.01;
        constraints.weighty = 0.02;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        bgPanel.add(subjectLabel, constraints);

        JTextArea subjectLine = new JTextArea();
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.gridheight = 1;
        constraints.weightx = 0.99;
        constraints.weighty = 0.02;
        constraints.fill = GridBagConstraints.BOTH;
        subjectLine.setRows(1);
        bgPanel.add(subjectLine, constraints);

        JLabel bodyLabel = new JLabel("Body: ");
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridheight = 1;
        constraints.weightx = 0.01;
        constraints.weighty = 0.9;

        constraints.fill = GridBagConstraints.HORIZONTAL;
        bgPanel.add(bodyLabel, constraints);

        JTextArea bodyArea = new JTextArea();
        constraints.gridx = 1;
        constraints.gridy = 4;
        constraints.gridheight = 1;
        constraints.weightx = 0.99;
        constraints.weighty = 0.9;
        constraints.fill = GridBagConstraints.BOTH;
        bodyArea.setRows(30);
        bgPanel.add(bodyArea, constraints);

        sendButton.addActionListener(e -> {
            for (String s : getEmails(fromLine.getText())) {
                bodyArea.append(s + "\n");
            }
            System.out.println(isValidSubject(subjectLine.getText()));
        });

        JLabel footerLabel = new JLabel("Footer: ");
        constraints.gridx = 0;
        constraints.gridy = 5;
        constraints.gridheight = 1;
        constraints.weightx = 0.01;
        constraints.weighty = 0.3;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        bgPanel.add(footerLabel, constraints);

        JTextArea footerArea = new JTextArea();
        constraints.gridx = 1;
        constraints.gridy = 5;
        constraints.gridheight = 1;
        constraints.weightx = 0.99;
        constraints.weighty = 0.3;
        constraints.fill = GridBagConstraints.BOTH;
        bgPanel.add(footerArea, constraints);
    }

    public void start(){
        mainFrame.setVisible(true);
    }

    public HashSet<String> getEmails(String s) {
        HashSet<String> result = new HashSet<>();
        boolean isValidEmail;
        String  adress,
                regEx = "(((?<NameBegin>[\\w\\\\.-]+\\s<)?"+
                        "(?<Adress>[\\w\\\\.-]+@[\\w]+[\\\\.][\\w]+([\\\\.][\\w]+)?)"+
                        "(?<NameEnd>>)?)(,\\s)?)+?";

        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(s);
        isValidEmail = matcher.matches();
        if (!isValidEmail) {
            return result;
        }
        matcher.reset();
        while (matcher.find()) {
            try {
                if (((matcher.group("NameBegin") != null
                        && matcher.group("NameEnd") == null))
                        || ((matcher.group("NameBegin") == null
                        && matcher.group("NameEnd") != null))) {
                    isValidEmail = false;
                }
            } catch (IllegalStateException e) {
                isValidEmail = false;
            }
            if (isValidEmail) {
                adress = matcher.group("Adress");
                result.add(adress);
            }
        }
        if (!isValidEmail) {
            result.clear();
        }
        return result;
    }

    public boolean isValidSubject(String s) {
        boolean result = false;
        String regEx = "[а-яёіўa-záéíñóúü\\s\\d\\p{Punct}]+";

        result = isUTF8Charset(s);
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(s);
        if (result) {
            result = matcher.matches();
        }
        return result;
    }

    public boolean isUTF8Charset(String s) {
        boolean result = false;
        CharsetDecoder csUTF8 = Charset.forName("UTF-8").newDecoder();
        try {
            csUTF8.decode(ByteBuffer.wrap(s.getBytes()));
            result = true;
        } catch (CharacterCodingException e) {

        }
        return result;
    }
}
